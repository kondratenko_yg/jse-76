package ru.kondratenko.tm.dto.response.list;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.response.ResponseDTO;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ListTaskResponseDTO extends ResponseDTO {
    private TaskDTO[] payloadTask;
}
