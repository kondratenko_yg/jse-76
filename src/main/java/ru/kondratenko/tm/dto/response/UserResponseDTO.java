package ru.kondratenko.tm.dto.response;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import ru.kondratenko.tm.dto.UserDTO;

@Data
@SuperBuilder
public class UserResponseDTO extends ResponseDTO {
    private UserDTO payloadUser;
}
