package ru.kondratenko.tm.dto.response;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import ru.kondratenko.tm.dto.TaskDTO;

@Data
@SuperBuilder
public class TaskResponseDTO extends ResponseDTO {
    private TaskDTO payloadTask;
}
