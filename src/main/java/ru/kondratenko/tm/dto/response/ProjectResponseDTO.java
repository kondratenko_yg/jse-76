package ru.kondratenko.tm.dto.response;

import lombok.Data;
import lombok.experimental.SuperBuilder;
import ru.kondratenko.tm.dto.ProjectDTO;

@Data
@SuperBuilder
public class ProjectResponseDTO extends ResponseDTO  {
    private ProjectDTO payloadProject;
}
