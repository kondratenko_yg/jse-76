package ru.kondratenko.tm.dto.mapper;

import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.entity.UserTM;

public class UserDTOMapper {
    public static UserDTO toDto(UserTM user) {
        UserDTO userDTO = UserDTO.builder()
                .id(user.getId())
                .name(user.getName())
                .password(user.getPassword())
                .firstName(user.getFirstname())
                .lastName(user.getLastname())
                .role(user.getRole())
                .build();
        return userDTO;
    }
}
