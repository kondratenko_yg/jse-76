package ru.kondratenko.tm.repository.JPA;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.UserTM;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<UserTM, Long> {
    Optional<UserTM> findByName(final String login);

    @Query("SELECT t FROM UserTM u inner join u.tasks as t where u.id = :id")
    List<Task> getTasks(@Param("id") Long id);

    @Query("SELECT t FROM UserTM u inner join u.projects as t where u.id = :id")
    List<Project> getProjects(@Param("id") Long id);

    @Query(
            value = "SELECT * FROM " +
                    "( SELECT *, row_number()  OVER () as rnum from " +
                    "users) as u where rnum = :index",
            nativeQuery = true)
    Optional<UserTM> getByIndex(@Param("index") Integer index);

    @Query(
            value = "DELETE FROM users WHERE user_id in " +
                    "(SELECT u.user_id  FROM" +
                    " (SELECT user_id, row_number()  OVER () as rnum from users) as u" +
                    " WHERE u.rnum = :index)",
            nativeQuery = true)
    @Modifying
    void removeByIndex(@Param("index") Integer index);

    void deleteByName(String name);
}
