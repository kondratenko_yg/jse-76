package ru.kondratenko.tm.repository.JPA;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kondratenko.tm.entity.Project;

import java.util.List;
import java.util.Optional;


public interface ProjectRepository extends JpaRepository<Project, Long> {
    List<Project> findByName(final String login);

    @Query("SELECT u FROM Project u inner join u.user as c_user where c_user.id = :id")
    List<Project> findAllByUserId(@Param("id") Long id);

    @Query(
            value = "SELECT * FROM " +
                    "( SELECT *, row_number()  OVER () as rnum from " +
                    "projects) as u where rnum = :index",
            nativeQuery = true)
    Optional<Project> getByIndex(@Param("index") Integer index);

    @Query(
            value = "DELETE FROM projects WHERE project_id in " +
                    "(SELECT u.project_id  FROM" +
                    " (SELECT project_id, row_number()  OVER () as rnum from projects) as u" +
                    " WHERE u.rnum = :index)",
            nativeQuery = true)
    @Modifying
    void removeByIndex(@Param("index") Integer index);

    void deleteByName(String name);
}
