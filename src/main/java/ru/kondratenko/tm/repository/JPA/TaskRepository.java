package ru.kondratenko.tm.repository.JPA;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kondratenko.tm.entity.Task;

import java.util.List;
import java.util.Optional;


public interface TaskRepository extends JpaRepository<Task, Long> {
    List<Task> findByName(final String login);

    @Query("SELECT u FROM Task u inner join u.user as c_user where c_user.id = :id")
    List<Task> findAllByUserId(@Param("id") Long id);

    @Query("SELECT u FROM Task u inner join u.project as c_project where u.id = :id and c_project.id=:project_id")
    Optional<Task> findByProjectIdAndId(@Param("project_id") Long projectId, @Param("id") Long id);

    List<Task> findAllByProjectId(Long projectId);

    @Query(
            value = "SELECT * FROM " +
                    "( SELECT *, row_number()  OVER () as rnum from " +
                    "tasks) as u where rnum = :index",
            nativeQuery = true)
    Optional<Task> getByIndex(@Param("index") Integer index);

    @Query(
            value = "DELETE FROM tasks WHERE task_id in " +
                    "(SELECT u.task_id  FROM" +
                    " (SELECT task_id, row_number()  OVER () as rnum from tasks) as u" +
                    " WHERE u.rnum = :index)",
            nativeQuery = true)
    @Modifying
    void removeByIndex(@Param("index") Integer index);

    void deleteByName(String name);

}
