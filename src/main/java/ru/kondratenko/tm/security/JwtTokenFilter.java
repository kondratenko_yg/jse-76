package ru.kondratenko.tm.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtTokenFilter extends OncePerRequestFilter {

    private JwtTokenProvider jwtTokenProvider;
    private UsersDetailsService usersDetailsService;

    @Autowired
    public JwtTokenFilter(JwtTokenProvider jwtTokenProvider, UsersDetailsService usersDetailsService) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.usersDetailsService = usersDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
                                    FilterChain filterChain) throws ServletException, IOException {

        String token = httpServletRequest.getHeader(jwtTokenProvider.getJwtHeader());

        if (token == null || !token.startsWith(jwtTokenProvider.getJwtPrefix())) {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
            return;
        }

        token = token.replace(jwtTokenProvider.getJwtPrefix(), "");

        String employee =  jwtTokenProvider.validateAndGetUserFromToken(token);

        if (employee != null) {
            UserDetails userDetails = usersDetailsService.loadUserByUsername(employee);

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
                    usersDetailsService, null, userDetails.getAuthorities());

            SecurityContextHolder.getContext().setAuthentication(authToken);
        }
        else {
            SecurityContextHolder.clearContext();
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

}
