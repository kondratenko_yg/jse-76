package ru.kondratenko.tm.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.kondratenko.tm.entity.UserTM;
import ru.kondratenko.tm.enumerated.Role;
import ru.kondratenko.tm.repository.JPA.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UsersDetailsService implements UserDetailsService {

    private UserRepository userRepository;

    @Autowired
    public UsersDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserTM> user = userRepository.findByName(username);

        if (user.isEmpty()) {
            throw new UsernameNotFoundException(username + " not found.");
        }

        List<SimpleGrantedAuthority> roleList = new ArrayList<>(1);
        if(user.get().getRole() == Role.USER) {
            roleList.add(new SimpleGrantedAuthority("ROLE_USER"));
        }

        if(user.get().getRole() == Role.ADMIN) {
            roleList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
        }

        return new User(user.get().getName(), user.get().getPassword(), roleList);
    }

}
