package ru.kondratenko.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kondratenko.tm.dto.ProjectDTO;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.dto.mapper.ProjectDTOMapper;
import ru.kondratenko.tm.dto.mapper.TaskDTOMapper;
import ru.kondratenko.tm.dto.mapper.UserDTOMapper;
import ru.kondratenko.tm.dto.response.UserResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListUserResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.entity.UserTM;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.repository.JPA.UserRepository;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Transactional
public class UserService implements IUserIService {

    private UserRepository userRepository;

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserResponseDTO create(final UserDTO userDTO) {
        final String login = userDTO.getName(), password = userDTO.getPassword();
        if (login == null || login.isEmpty() || password == null || password.isEmpty()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        Optional<UserTM> user = userRepository.findByName(login);
        if(user.isEmpty()){
            UserTM newUser = UserTM.builder()
                    .name(userDTO.getName())
                    .password(passwordEncoder.encode(userDTO.getPassword()))
                    .firstname(userDTO.getFirstName())
                    .lastname(userDTO.getLastName())
                    .role(userDTO.getRole()).build();
            userRepository.save(newUser);
            return UserResponseDTO.builder().status(Status.OK).payloadUser(UserDTOMapper.toDto(newUser)).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateByIndex(final int index, final UserDTO userDTO) {
        final String login = userDTO.getName();
        final String password = userDTO.getPassword();
        Optional<String> message = checkLoginAndPassword(login, password);
        if (message.isPresent() || index < 0) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).textError(message.isEmpty() ? "Index<0!" : message.get()).build();
        }
        Optional<UserTM> user1 = userRepository.getByIndex(index);
        if (user1.isPresent()) {
            UserTM userUpdate = user1.get();
            userUpdate.setId(user1.get().getId());
            userUpdate.setName(userDTO.getName());
            userUpdate.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            userUpdate.setFirstname(userDTO.getFirstName());
            userUpdate.setLastname(userDTO.getLastName());
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user1.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateById(final long id, final UserDTO userDTO) {
        final String login = userDTO.getName();
        final String password = userDTO.getPassword();
        Optional<String> message = checkLoginAndPassword(login, password);
        if (message.isPresent()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).textError(message.get()).build();
        }
        Optional<UserTM> user1 = userRepository.findById(id);
        if (user1.isPresent()) {
            UserTM userUpdate = user1.get();
            userUpdate.setId(user1.get().getId());
            userUpdate.setName(userDTO.getName());
            userUpdate.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            userUpdate.setFirstname(userDTO.getFirstName());
            userUpdate.setLastname(userDTO.getLastName());
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user1.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public UserResponseDTO updateByName(final String name, final UserDTO userDTO) {
        Optional<String> message = checkLoginAndPassword(userDTO.getName(), userDTO.getPassword());
        if (message.isPresent()) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).textError(message.get()).build();
        }
        Optional<UserTM> user1 = userRepository.findByName(name);
        if (user1.isPresent()) {
            UserTM userUpdate = user1.get();
            userUpdate.setId(user1.get().getId());
            userUpdate.setName(userDTO.getName());
            userUpdate.setPassword(passwordEncoder.encode(userDTO.getPassword()));
            userUpdate.setFirstname(userDTO.getFirstName());
            userUpdate.setLastname(userDTO.getLastName());
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user1.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public boolean checkPassword(final Optional<UserTM> user, final String password) {
        return user.map(value -> value.getPassword().equals(passwordEncoder.encode(password))).orElse(false);
    }

    @Override
    public UserResponseDTO findByName(String login) {
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<UserTM> user = userRepository.findByName(login);
        if(user.isPresent()){
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).textError("User is not found.").build();
    }

    @Override
    public UserResponseDTO findById(Long id) {
        if (id == null) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<UserTM> user = userRepository.findById(id);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).textError("User is not found.").build();
    }

    @Override
    public UserResponseDTO findByIndex(final int index) {
        if (index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<UserTM> user = userRepository.getByIndex(index);
        if (user.isPresent()) {
            return UserResponseDTO.builder().payloadUser(UserDTOMapper.toDto(user.get())).status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).textError("User is not found.").build();
    }

    @Override
    public UserResponseDTO removeByName(String login) {
        if (login == null || login.isEmpty()) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<UserTM> user = userRepository.findByName(login);
        if(user.isPresent()) {
            userRepository.deleteByName(login);
            return UserResponseDTO.builder().status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).textError("User is not found.").build();
    }

    @Override
    public ListTaskResponseDTO findTasks(Long id) {
        if (id == null) return ListTaskResponseDTO.builder().status(Status.DB_ERROR).build();
        List<Task> tasks = userRepository.getTasks(id);
        return ListTaskResponseDTO
                .builder()
                .payloadTask(tasks.stream().map(TaskDTOMapper::toDto).toArray(TaskDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO findProjects(Long id) {
        if (id == null) return ListProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        List<Project> projects = userRepository.getProjects(id);
        return ListProjectResponseDTO
                .builder()
                .payloadProject(projects.stream().map(ProjectDTOMapper::toDto).toArray(ProjectDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public UserResponseDTO logOff() {
        SecurityContextHolder.getContext().getAuthentication().setAuthenticated(false);
        return UserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public UserResponseDTO removeById(Long id)  {
        if (id == null) {
            return UserResponseDTO.builder().status(Status.DB_ERROR).textError("Id is null.").build();
        }
        Optional<UserTM> user = userRepository.findById(id);
        if(user.isPresent()) {
            userRepository.deleteById(id);
            return UserResponseDTO.builder().status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).textError("User is not found.").build();
    }

    @Override
    public UserResponseDTO removeByIndex(Integer index) {
        if (index < 0) return UserResponseDTO.builder().status(Status.DB_ERROR).build();
        Optional<UserTM> user = userRepository.getByIndex(index);
        if (user.isPresent()) {
            userRepository.removeByIndex(index);
            return UserResponseDTO.builder().status(Status.OK).build();
        }
        return UserResponseDTO.builder().status(Status.DB_ERROR).textError("User is not found.").build();

    }

    @Override
    public void clear() {
        userRepository.deleteAll();
    }

    @Override
    public ListUserResponseDTO findAll() {
        return ListUserResponseDTO
                .builder()
                .payloadUser(userRepository.findAll().stream().map(UserDTOMapper::toDto).toArray(UserDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO saveJSON(final String fileName) throws IOException {
        writeJSON(fileName, userRepository.findAll().stream().map(UserDTOMapper::toDto).collect(Collectors.toList()));
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO saveXML(final String fileName) throws IOException {
        writeXML(fileName, userRepository.findAll().stream().map(UserDTOMapper::toDto).collect(Collectors.toList()));
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO uploadFromJSON(final String fileName) throws IOException {
        List<UserDTO> users = uploadJSONToList(fileName, UserDTO.class);
        clear();
        users.forEach(this::create);
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListUserResponseDTO uploadFromXML(final String fileName) throws IOException {
        List<UserDTO> users = uploadXMLToList(fileName, UserDTO.class);
        clear();
        users.forEach(this::create);
        return ListUserResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public String getCurrentUser() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    private Optional<String> checkLoginAndPassword(String login, String password) {
        if (login == null || login.isEmpty()) {
            return Optional.of("Login is not correct!");
        }
        if (password == null) {
            return Optional.of("Password is not correct!");
        }
        return Optional.empty();
    }
}
