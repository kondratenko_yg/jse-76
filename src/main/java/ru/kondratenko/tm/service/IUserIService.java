package ru.kondratenko.tm.service;

import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.dto.response.UserResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListUserResponseDTO;
import ru.kondratenko.tm.entity.UserTM;

import java.util.Optional;

public interface IUserIService extends IService<UserDTO, UserResponseDTO, ListUserResponseDTO> {
    UserResponseDTO updateByName(final String name,final UserDTO user);
    boolean checkPassword(final Optional<UserTM> user, final String password);
    String getCurrentUser();
    UserResponseDTO findByName(final String name);
    UserResponseDTO removeByName(final String name);
    ListTaskResponseDTO findTasks(Long id);
    ListProjectResponseDTO findProjects(Long id);
    UserResponseDTO logOff();
}
