package ru.kondratenko.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.kondratenko.tm.dto.ProjectDTO;
import ru.kondratenko.tm.dto.mapper.ProjectDTOMapper;
import ru.kondratenko.tm.dto.response.ProjectResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.entity.UserTM;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.repository.JPA.ProjectRepository;
import ru.kondratenko.tm.repository.JPA.UserRepository;
import ru.kondratenko.tm.util.Helper;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ProjectService implements IProjectIService {

    private ProjectRepository projectRepository;

    private UserRepository userRepository;

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Autowired
    public void setProjectRepository(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public ProjectResponseDTO create(final ProjectDTO projectDTO) {
        String name = projectDTO.getName();
        if (name.equals("") || Helper.checkProjectName(name) || name.isEmpty()) {
            return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError("Bad name.").build();
        }
        Project project;
        try {
            UserTM user = projectDTO.getUserId() == null ? null : userRepository.findById(projectDTO.getUserId()).isEmpty() ? null :
                    userRepository.findById(projectDTO.getUserId()).get();
            project = Project.builder()
                    .name(projectDTO.getName())
                    .description(projectDTO.getDescription())
                    .user(user)
                    .build();
        } catch (Exception e) {
            return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError(e.getMessage()).build();
        }
        projectRepository.save(project);
        return ProjectResponseDTO.builder().payloadProject(ProjectDTOMapper.toDto(project)).status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO updateByIndex(final int index, ProjectDTO projectDTO) {
        Optional<Project> project1 = projectRepository.getByIndex(index);
        if (project1.isEmpty()) {
            return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError("Element is not found!").build();
        }
        Project updatedProject;
        try {
            updatedProject = Project.builder()
                    .id(project1.get().getId())
                    .name(projectDTO.getName())
                    .description(projectDTO.getDescription())
                    .user(projectDTO.getUserId() == null ? null : userRepository.findById(projectDTO.getUserId()).isEmpty() ? null :
                            userRepository.findById(projectDTO.getUserId()).get())
                    .build();
        } catch (Exception e) {
            return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError(e.getMessage()).build();
        }
        return ProjectResponseDTO.builder().payloadProject(ProjectDTOMapper.toDto(updatedProject)).status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO updateById(final long id, ProjectDTO projectDTO) {
        Optional<Project> project1 = projectRepository.findById(id);
        if (project1.isEmpty()) {
            return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError("Element is not found!").build();
        }
        Project updatedProject;
        try {
            updatedProject = Project.builder()
                    .id(project1.get().getId())
                    .name(projectDTO.getName())
                    .description(projectDTO.getDescription())
                    .user(projectDTO.getUserId() == null ? null : userRepository.findById(projectDTO.getUserId()).isEmpty() ? null :
                            userRepository.findById(projectDTO.getUserId()).get())
                    .build();
        } catch (Exception e) {
            return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError(e.getMessage()).build();
        }
        return ProjectResponseDTO.builder().payloadProject(ProjectDTOMapper.toDto(updatedProject)).status(Status.OK).build();

    }


    @Override
    public ProjectResponseDTO findByIndex(final int index) {
        Optional<Project> project1 = projectRepository.getByIndex(index);
        if (project1.isPresent()) {
            return ProjectResponseDTO.builder().payloadProject(ProjectDTOMapper.toDto(project1.get())).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).build();
    }

    @Override
    public ListProjectResponseDTO findByName(final String name) {
        List<Project> project1 = projectRepository.findByName(name);
        if (project1.isEmpty()) {
            return ListProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        }
        return ListProjectResponseDTO
                .builder()
                .payloadProject(project1.stream().map(ProjectDTOMapper::toDto).toArray(ProjectDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ProjectResponseDTO findById(final Long id) {
        Optional<Project> project1 = projectRepository.findById(id);
        if (project1.isPresent()) {
            return ProjectResponseDTO.builder().payloadProject(ProjectDTOMapper.toDto(project1.get())).status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError("Project is not found.").build();
    }

    @Override
    public ProjectResponseDTO removeByIndex(final Integer index) {
        Optional<Project> project1 = projectRepository.getByIndex(index);
        if (project1.isPresent()) {
            projectRepository.removeByIndex(index);
            return ProjectResponseDTO.builder().status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError("Project is not found.").build();
    }

    @Override
    public ProjectResponseDTO removeById(final Long id) {
        Optional<Project> project1 = projectRepository.findById(id);
        if (project1.isPresent()) {
            projectRepository.deleteById(id);
            return ProjectResponseDTO.builder().status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError("Project is not found.").build();
    }

    @Override
    public ProjectResponseDTO removeByName(final String name) {
        List<Project> project1 = projectRepository.findByName(name);
        if (project1.isEmpty()) {
            projectRepository.deleteByName(name);
            return ProjectResponseDTO.builder().status(Status.OK).build();
        }
        return ProjectResponseDTO.builder().status(Status.DB_ERROR).textError("Project is not found.").build();
    }

    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    public ListProjectResponseDTO findAll() {
        return ListProjectResponseDTO
                .builder()
                .payloadProject(projectRepository.findAll().stream().map(ProjectDTOMapper::toDto).toArray(ProjectDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO findAllByUserId(Long userId) {
        if (userId == null) return ListProjectResponseDTO.builder().status(Status.DB_ERROR).build();
        return ListProjectResponseDTO
                .builder()
                .payloadProject(projectRepository.findAllByUserId(userId).stream().map(ProjectDTOMapper::toDto).toArray(ProjectDTO[]::new))
                .status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO saveJSON(final String fileName) throws IOException {
        writeJSON(fileName, projectRepository.findAll().stream().map(ProjectDTOMapper::toDto).collect(Collectors.toList()));
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO saveXML(final String fileName) throws IOException {
        writeXML(fileName, projectRepository.findAll().stream().map(ProjectDTOMapper::toDto).collect(Collectors.toList()));
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO uploadFromJSON(final String fileName) throws IOException {
        List<ProjectDTO> projects = uploadJSONToList(fileName, ProjectDTO.class);
        clear();
        projects.forEach(this::create);
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }

    @Override
    public ListProjectResponseDTO uploadFromXML(final String fileName) throws IOException {
        List<ProjectDTO> projects = uploadXMLToList(fileName, ProjectDTO.class);
        clear();
        projects.forEach(this::create);
        return ListProjectResponseDTO.builder().status(Status.OK).build();
    }
}
