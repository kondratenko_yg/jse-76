package ru.kondratenko.tm.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kondratenko.tm.dto.UserDTO;
import ru.kondratenko.tm.dto.response.UserResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListUserResponseDTO;
import ru.kondratenko.tm.service.IUserIService;


@RestController
@RequestMapping("/user")
public class UserController implements IControllerUser {

    private IUserIService userService;

    @Autowired
    public UserController(IUserIService userService) {
        this.userService = userService;
    }

    @Override
    @ApiOperation("Пользователь по идентификатору")
    @GetMapping(value = "/{id}", produces = "application/json")
    public UserResponseDTO viewById(@PathVariable Long id)  {
        return userService.findById(id);
    }

    @Override
    @ApiOperation("Все пользователи")
    @GetMapping(value = "/all", produces = "application/json")
    public ListUserResponseDTO list()  {
        return userService.findAll();
    }

    @Override
    @ApiOperation("Все задачи пользователя")
    @GetMapping(value = "/{id}/tasks", produces = "application/json")
    public ListTaskResponseDTO findTasks(@PathVariable Long id) {
        return userService.findTasks(id);
    }

    @Override
    @ApiOperation("Все проекты пользователя")
    @GetMapping(value = "/{id}/projects", produces = "application/json")
    public ListProjectResponseDTO findProjects(@PathVariable Long id) {
        return userService.findProjects(id);
    }

    @Override
    @ApiOperation("Пользователь по индексу в базе")
    @GetMapping(value = "/view/index/{index}", produces = "application/json")
    public UserResponseDTO viewByIndex(@PathVariable Integer index) {
        return userService.findByIndex(index);
    }

    @Override
    @ApiOperation("Пользователь по наименованию")
    @GetMapping(value = "/view/name/{name}", produces = "application/json")
    public UserResponseDTO viewByName(@PathVariable String name) {
        return userService.findByName(name);
    }

    @Override
    @ApiOperation("Создать пользователя")
    @PostMapping(value = "/create", produces = "application/json",consumes = "application/json")
    public UserResponseDTO create(@RequestBody UserDTO user) {
        return userService.create(user);
    }

    @Override
    @ApiOperation("Изменить пользователя по индексу")
    @PutMapping(value ="/update/index/{index}", produces = "application/json",consumes = "application/json")
    public UserResponseDTO updateByIndex(@PathVariable Integer index,@RequestBody UserDTO user) {
        return userService.updateByIndex(index,user);
    }

    @Override
    @ApiOperation("Изменить пользователя по идентификатору")
    @PutMapping(value ="/update/id/{id}", produces = "application/json",consumes = "application/json")
    public UserResponseDTO updateById(@PathVariable Long id,@RequestBody UserDTO user) {
        return userService.updateById(id,user);
    }

    @Override
    @ApiOperation("Удалить пользователя по идентификатору")
    @DeleteMapping(value ="/remove/id/{id}", produces = "application/json")
    public UserResponseDTO removeById(@PathVariable Long id) {
        return userService.removeById(id);
    }

    @Override
    @ApiOperation("Удалить пользователя по индексу")
    @DeleteMapping(value ="/remove/index/{index}", produces = "application/json")
    public UserResponseDTO removeByIndex(@PathVariable Integer index) {
        return userService.removeByIndex(index);
    }

    @Override
    @ApiOperation("Выход")
    @GetMapping(value = "/logOff")
    public UserResponseDTO logOff() {
        return userService.logOff();
    }

}
