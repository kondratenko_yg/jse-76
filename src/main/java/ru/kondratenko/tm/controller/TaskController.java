package ru.kondratenko.tm.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kondratenko.tm.dto.TaskDTO;
import ru.kondratenko.tm.dto.response.TaskResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListTaskResponseDTO;
import ru.kondratenko.tm.service.IProjectTaskService;
import ru.kondratenko.tm.service.ITaskIService;


@RestController
@RequestMapping("/task")
public class TaskController implements IControllerTask {

    private IProjectTaskService projectTaskService;

    private ITaskIService taskService;

    @Autowired
    public TaskController(ITaskIService taskService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    @ApiOperation("Задача по идентификатору")
    @GetMapping(value = "/{id}", produces = "application/json")
    public TaskResponseDTO viewById( @ApiParam (value ="Идентификатор задачи")@PathVariable Long id)  {
        return taskService.findById(id);
    }

    @Override
    @ApiOperation("Все задачи")
    @GetMapping(value = "/all", produces = "application/json")
    public ListTaskResponseDTO list()  {
        return taskService.findAll();
    }

    @Override
    @ApiOperation("Задача по индексу в базе")
    @GetMapping(value = "/view/index/{index}", produces = "application/json")
    public TaskResponseDTO viewByIndex(@PathVariable Integer index) {
        return taskService.findByIndex(index);
    }

    @Override
    @ApiOperation("Задача по наименованию")
    @GetMapping(value = "/view/name/{name}", produces = "application/json")
    public ListTaskResponseDTO viewByName(@PathVariable String name) {
        return taskService.findByName(name);
    }

    @Override
    @ApiOperation("Создать задачу")
    @PostMapping(value = "/create", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO create(@RequestBody TaskDTO task) {
        return taskService.create(task);
    }

    @Override
    @ApiOperation("Изменить задачу по индексу")
    @PutMapping(value ="/update/index/{index}", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO updateByIndex(@ApiParam (value ="Индекс в базе") @PathVariable Integer index,@RequestBody TaskDTO task) {
        return taskService.updateByIndex(index,task);
    }

    @Override
    @ApiOperation("Все задачи проекта")
    @GetMapping(value = "/view", produces = "application/json")
    public ListTaskResponseDTO listTaskByProjectId(@ApiParam (value ="Идентификатор проекта") @RequestParam(name = "projectid") Long projectId) {
        return taskService.findAllByProjectId(projectId);
    }

    @Override
    @ApiOperation("Найти задачу по идентификатору задачи и проекта, к которому она относится")
    @GetMapping(value = "{taskId}/view/project/{projectId}", produces = "application/json")
    public TaskResponseDTO findByProjectIdAndId(
            @ApiParam (value ="Идентификатор задачи") @PathVariable Long taskId,
            @ApiParam (value ="Идентификатор проекта") @PathVariable Long projectId) {
        return taskService.findByProjectIdAndId(projectId,taskId);
    }

    @Override
    @ApiOperation("Добавить задачу к проекту")
    @PutMapping(value = "/add/toproject", produces = "application/json")
    public TaskResponseDTO addTaskToProjectByIds(
            @ApiParam (value ="Идентификатор проекта") @RequestParam(name = "projectid") Long projectId,
            @ApiParam (value ="Идентификатор задачи") @RequestParam(name = "taskid") Long taskId) {
        return projectTaskService.addTaskToProject(projectId,taskId);
    }

    @Override
    @ApiOperation("Удаление задачи из проекта")
    @PutMapping(value = "/remove/fromproject", produces = "application/json")
    public TaskResponseDTO removeTaskFromProjectByIds(
            @ApiParam (value ="Идентификатор проекта") @RequestParam(name = "projectid") Long projectId,
            @ApiParam (value ="Идентификатор задачи")  @RequestParam(name = "taskid") Long taskId) {
        return projectTaskService.removeTaskFromProject(projectId,taskId);
    }

    @Override
    @ApiOperation("Изменить задачу по идентификатору")
    @PutMapping(value ="/update/id/{id}", produces = "application/json",consumes = "application/json")
    public TaskResponseDTO updateById( @ApiParam (value ="Идентификатор задачи") @PathVariable Long id,@RequestBody TaskDTO task) {
        return taskService.updateById(id,task);
    }

    @Override
    @ApiOperation("Удалить задачу по идентификатору")
    @DeleteMapping(value ="/remove/id/{id}", produces = "application/json")
    public TaskResponseDTO removeById( @ApiParam (value ="Идентификатор задачи") @PathVariable Long id) {
        return taskService.removeById(id);
    }

    @Override
    @ApiOperation("Удалить задачу по индексу")
    @DeleteMapping(value ="/remove/index/{index}", produces = "application/json")
    public TaskResponseDTO removeByIndex(@ApiParam (value ="Индекс в базе") @PathVariable Integer index) {
        return taskService.removeByIndex(index);
    }

    @Override
    @ApiOperation("Найти все задачи по идентификатору пользователя")
    @GetMapping(value = "/view/userid/{id}", produces = "application/json")
    public ListTaskResponseDTO findAllByUserId(@ApiParam (value ="Идентификатор пользователя")  @PathVariable Long id) {
        return taskService.findAllByUserId(id);
    }

}
