package ru.kondratenko.tm.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.kondratenko.tm.dto.ProjectDTO;
import ru.kondratenko.tm.dto.response.ProjectResponseDTO;
import ru.kondratenko.tm.dto.response.list.ListProjectResponseDTO;
import ru.kondratenko.tm.service.IProjectIService;


@RestController
@RequestMapping("/project")
public class ProjectController implements IControllerProject {

    private IProjectIService projectService;

    @Autowired
    public ProjectController(IProjectIService projectService) {
        this.projectService = projectService;
    }

    @Override
    @ApiOperation("Проект по идентификатору")
    @GetMapping(value = "/{id}", produces = "application/json")
    public ProjectResponseDTO viewById(@PathVariable Long id)  {
        return projectService.findById(id);
    }

    @Override
    @ApiOperation("Все проекты")
    @GetMapping(value = "/all", produces = "application/json")
    public ListProjectResponseDTO list()  {
        return projectService.findAll();
    }

    @Override
    @ApiOperation("Проект по индексу в базе")
    @GetMapping(value = "/view/index/{index}", produces = "application/json")
    public ProjectResponseDTO viewByIndex(@PathVariable Integer index) {
        return projectService.findByIndex(index);
    }

    @Override
    @ApiOperation("Проект по наименованию")
    @GetMapping(value = "/view/name/{name}", produces = "application/json")
    public ListProjectResponseDTO viewByName(@PathVariable String name) {
        return projectService.findByName(name);
    }

    @Override
    @ApiOperation("Создать проект")
    @PostMapping(value = "/create", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO create(@RequestBody ProjectDTO project) {
        return projectService.create(project);
    }

    @Override
    @ApiOperation("Изменить проект по индексу")
    @PutMapping(value ="/update/index/{index}", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO updateByIndex(@PathVariable Integer index,@RequestBody ProjectDTO project) {
        return projectService.updateByIndex(index,project);
    }

    @Override
    @ApiOperation("Изменить проект по идентификатору")
    @PutMapping(value ="/update/id/{id}", produces = "application/json",consumes = "application/json")
    public ProjectResponseDTO updateById(@PathVariable Long id,@RequestBody ProjectDTO project) {
        return projectService.updateById(id,project);
    }

    @Override
    @ApiOperation("Удалить проект по идентификатору")
    @DeleteMapping(value ="/remove/id/{id}", produces = "application/json")
    public ProjectResponseDTO removeById(@PathVariable Long id) {
        return projectService.removeById(id);
    }

    @Override
    @ApiOperation("Удалить проект по индексу")
    @DeleteMapping(value ="/remove/index/{index}", produces = "application/json")
    public ProjectResponseDTO removeByIndex(@PathVariable Integer index) {
        return projectService.removeByIndex(index);
    }

    @Override
    @ApiOperation("Найти все проекты по идентификатору пользователя")
    @ApiResponses(value = {
            @ApiResponse(code=200,message = "Проекты найдены"),
            @ApiResponse(code=400,message = "Неверный идентификатор")
    })
    @GetMapping(value = "/view/userid/{id}", produces = "application/json")
    public ListProjectResponseDTO findAllByUserId(@PathVariable Long id) {
        return projectService.findAllByUserId(id);
    }

}
