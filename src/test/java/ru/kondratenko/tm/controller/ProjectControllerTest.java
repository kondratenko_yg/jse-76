package ru.kondratenko.tm.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import ru.kondratenko.tm.entity.Project;
import ru.kondratenko.tm.repository.JPA.ProjectRepository;

public class ProjectControllerTest extends AbstractControllerTest {

    @Override
    public String getControllerType(){
        return "Project";
    }

    private ProjectRepository projectRepository;

    @Test
    public void testGetById() throws Exception {
        super.testGetById("2");
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        MvcResult mvcResult = super.testUpdateByIndex( "\"userId\":\"1\",\"description\":\""+DESCRIPTION_PROJECT+"\"");
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(DESCRIPTION_PROJECT));
    }

    @Test
    public void testUpdateById() throws Exception {
        MvcResult mvcResult = super.testUpdateById( "\"userId\":\"1\",\"description\":\""+DESCRIPTION_PROJECT+1+"\"","2");
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(DESCRIPTION_PROJECT+1));
    }

    @Test
    public void testRemoveById() throws Exception {
        projectRepository = (ProjectRepository) webApplicationContext.getBean("projectRepository");
        Project project = new Project(NAME_CREATE_PROJECT+"ToDelete");
        projectRepository.save(project);
        super.testRemoveById(project.getId().toString());
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        projectRepository = (ProjectRepository) webApplicationContext.getBean("projectRepository");
        Project project = new Project(NAME_CREATE_PROJECT+"ToDeleteIndex");
        projectRepository.save(project);
        if(projectRepository.findAll().size() == 2) {
            super.testRemoveByIndex(2);
        }
    }

    @Test
    public void testViewByUserId() throws Exception {
        super.testViewByUserId();
    }
}
