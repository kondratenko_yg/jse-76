package ru.kondratenko.tm.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Locale;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestPropertySource(locations = "classpath:application.properties")
@WebAppConfiguration
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public abstract class AbstractControllerTest {

    public abstract String getControllerType();

    public static final String NAME_CREATE_PROJECT = "nameCreateProject";
    public static final String NAME_CREATE_USER = "nameCreateUser";
    public static final String NAME_CREATE_TASK = "nameCreateTask";
    private static final String NAME_CREATE = "nameCreate";

    public static final String DESCRIPTION_PROJECT = "descriptionProject";
    public static final String FIRST_NAME_USER = "nameUser";
    public static final String DESCRIPTION_TASK = "descriptionTask";

    @Autowired
    public WebApplicationContext webApplicationContext;

    public MockMvc mockMvc;

    @BeforeAll
    public void setup() throws Exception {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .defaultRequest(get("/").with(user("user").roles("ADMIN")))
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();
        if(!getById("1","user").getResponse().getContentAsString().contains(NAME_CREATE_USER)) {
            mockMvc.perform(post("/user/create")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"name\":\"" + NAME_CREATE_USER + "\",\"password\":\"123\"}"))
                    .andDo(print()).andExpect(status().isOk());
            mockMvc.perform(post("/project/create")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"name\":\"" + NAME_CREATE_PROJECT + "\"," +
                            "\"userId\":\"1\"}"))
                    .andDo(print()).andExpect(status().isOk());
            mockMvc.perform(post("/task/create")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{\"name\":\"" + NAME_CREATE_TASK + "\"," +
                            "\"userId\":\"1\"," +
                            "\"projectId\":\"2\"}"))
                    .andDo(print()).andExpect(status().isOk());
        }
    }

    @Test
    public void testGetAll() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/" + getControllerType().toLowerCase(Locale.ROOT) + "/all"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(NAME_CREATE + getControllerType()));
    }

    public  MvcResult getById(String id, String name) throws Exception {
        return mockMvc.perform(get("/" + name + "/" + id))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
    }

    public void testGetById(String id) throws Exception {
        Assertions.assertTrue(getById(id, getControllerType().toLowerCase(Locale.ROOT)).getResponse().getContentAsString().contains(NAME_CREATE + getControllerType()));
    }

    @Test
    public void testGetByIndex() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/" + getControllerType().toLowerCase(Locale.ROOT) + "/view/index/1"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(NAME_CREATE + getControllerType()));
    }

    @Test
    public void testGetByName() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/" + getControllerType().toLowerCase(Locale.ROOT) + "/view/name/" + NAME_CREATE + getControllerType()))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(NAME_CREATE + getControllerType()));
    }

    public MvcResult testUpdateByIndex(String change) throws Exception {
        return mockMvc.perform(put("/" + getControllerType().toLowerCase(Locale.ROOT) + "/update/index/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"" + NAME_CREATE + getControllerType() + "\"," + change + "}"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();

    }

    public MvcResult testUpdateById(String change, String id) throws Exception {
        return mockMvc.perform(put("/" + getControllerType().toLowerCase(Locale.ROOT) + "/update/id/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"name\":\"" + NAME_CREATE + getControllerType() + "\"," + change + "}"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();

    }

    public void testRemoveById(String id) throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/" + getControllerType().toLowerCase(Locale.ROOT) + "/remove/id/" + id))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains("OK"));
        MvcResult mvcResult1 = mockMvc.perform(get("/" + getControllerType().toLowerCase(Locale.ROOT) + "/all"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertFalse(mvcResult1.getResponse().getContentAsString().contains(NAME_CREATE + getControllerType() + "ToDelete"));
    }


    public void testRemoveByIndex(Integer index) throws Exception {
        MvcResult mvcResult = mockMvc.perform(delete("/" + getControllerType().toLowerCase(Locale.ROOT) + "/remove/index/" + index))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains("OK"));
        MvcResult mvcResult1 = mockMvc.perform(get("/" + getControllerType().toLowerCase(Locale.ROOT) + "/all"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertFalse(mvcResult1.getResponse().getContentAsString().contains(NAME_CREATE + getControllerType() + "ToDeleteIndex"));
    }

    public void testViewByUserId() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/" + getControllerType().toLowerCase(Locale.ROOT) + "/view/userid/1"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(NAME_CREATE + getControllerType()));
    }
}
