package ru.kondratenko.tm.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import ru.kondratenko.tm.entity.UserTM;
import ru.kondratenko.tm.repository.JPA.UserRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class UserControllerTest extends AbstractControllerTest {

    @Override
    public String getControllerType(){
        return  "User";
    }

    private UserRepository userRepository;

    @Test
    public void testGetById() throws Exception {
        super.testGetById("1");
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        MvcResult mvcResult = super.testUpdateByIndex(  "\"password\":\"123\",\"firstName\":\""+FIRST_NAME_USER+"\"");
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(FIRST_NAME_USER));
    }

    @Test
    public void testUpdateById() throws Exception {
        MvcResult mvcResult = super.testUpdateById( "\"password\":\"123\",\"firstName\":\""+FIRST_NAME_USER+1+"\"","1");
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(FIRST_NAME_USER+1));
    }

    @Test
    public void testRemoveById() throws Exception {
        userRepository = (UserRepository) webApplicationContext.getBean("userRepository");
        UserTM user  = new UserTM(NAME_CREATE_TASK+"ToDelete","123");
        userRepository.save(user);
        super.testRemoveById(user.getId().toString());
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        userRepository = (UserRepository) webApplicationContext.getBean("userRepository");
        UserTM user  = new UserTM(NAME_CREATE_TASK+"ToDeleteIndex","123");
        userRepository.save(user);
        if(userRepository.findAll().size() == 2) {
            super.testRemoveByIndex(2);
        }
    }

    @Test
    public void testViewProjectsAndTasks() throws Exception {
        MvcResult mvcResultProjects  = mockMvc.perform(get("/user/1/projects"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResultProjects.getResponse().getContentAsString().contains(NAME_CREATE_PROJECT));

        MvcResult mvcResultTasks  = mockMvc.perform(get("/user/1/tasks"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResultTasks.getResponse().getContentAsString().contains(NAME_CREATE_TASK));
    }
}
