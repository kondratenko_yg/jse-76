package ru.kondratenko.tm.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MvcResult;
import ru.kondratenko.tm.entity.Task;
import ru.kondratenko.tm.enumerated.Status;
import ru.kondratenko.tm.repository.JPA.TaskRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TaskControllerTest extends AbstractControllerTest {

    @Override
    public String getControllerType() {
        return "Task";
    }

    private TaskRepository taskRepository;

    @Test
    public void testGetById() throws Exception {
        super.testGetById("3");
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        MvcResult mvcResult = super.testUpdateByIndex("\"userId\":\"1\"," +
                "\"projectId\":\"2\"," + "\"description\":\"" + DESCRIPTION_TASK + "\"");
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(DESCRIPTION_TASK));
    }

    @Test
    public void testUpdateById() throws Exception {
        MvcResult mvcResult = super.testUpdateById("\"userId\":\"1\"," +
                "\"projectId\":\"2\"," + "\"description\":\"" + DESCRIPTION_TASK + 1 + "\"", "3");
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(DESCRIPTION_TASK + 1));
    }

    @Test
    public void testRemoveById() throws Exception {
        taskRepository = (TaskRepository) webApplicationContext.getBean("taskRepository");
        Task task = new Task(NAME_CREATE_TASK + "ToDelete");
        taskRepository.save(task);
        super.testRemoveById(task.getId().toString());
    }

    @Test
    public void testRemoveByIndex() throws Exception {
        taskRepository = (TaskRepository) webApplicationContext.getBean("taskRepository");
        Task task = new Task(NAME_CREATE_TASK + "ToDeleteIndex");
        taskRepository.save(task);
        if (taskRepository.findAll().size() == 2) {
            super.testRemoveByIndex(2);
        }
    }

    @Test
    public void testViewByUserId() throws Exception {
        super.testViewByUserId();
    }

    @Test
    public void testViewByProjectId() throws Exception {
        MvcResult mvcResult = mockMvc.perform(get("/task/view?projectid=2"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn();
        Assertions.assertTrue(mvcResult.getResponse().getContentAsString().contains(NAME_CREATE_TASK));
    }

    @Test
    public void testViewByProjectIdAndTaskId() throws Exception {
        Assertions.assertTrue(testTaskInProject().contains(NAME_CREATE_TASK));
        Assertions.assertTrue(mockMvc
                .perform(get("/task/12/view/project/2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString()
                .contains(Status.DB_ERROR.toString()));

        mockMvc.perform(put("/task/remove/fromproject?projectid=2&taskid=3"))
                .andDo(print()).andExpect(status().isOk());

        Assertions.assertFalse(testTaskInProject().contains(NAME_CREATE_TASK));

        mockMvc.perform(put("/task/add/toproject?projectid=2&taskid=3"))
                .andDo(print()).andExpect(status().isOk());

        Assertions.assertTrue(testTaskInProject().contains(NAME_CREATE_TASK));
    }

    public String testTaskInProject() throws Exception {
        return mockMvc.perform(get("/task/3/view/project/2"))
                .andDo(print()).andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
    }
}
